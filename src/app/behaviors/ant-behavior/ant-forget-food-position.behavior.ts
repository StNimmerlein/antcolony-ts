import { Injectable } from '@angular/core'
import { Ant, Area, PositionedEntity } from '../../model/Area'
import { AntBehavior } from './ant-behavior'

@Injectable({providedIn: 'root'})
export class AntForgetFoodPositionBehavior implements AntBehavior {
  terminating = false

  apply(ant: PositionedEntity<Ant>, area: Area): void {
    ant.entity.rememberedFoodPosition = undefined
  }

  getPriority(): number {
    return 104;
  }

  isApplicable(ant: PositionedEntity<Ant>, area: Area): boolean {
    return ant.position.equals(ant.entity.rememberedFoodPosition)
      && !area.getFieldAt(ant.position).entities.some(entity => entity.type === 'food');
  }
}
