import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BehaviorModule } from './behaviors/behavior.module'
import { AreaComponent } from './components/area/area.component';
import { FieldComponent } from './components/field/field.component';

@NgModule({
  declarations: [
    AppComponent,
    AreaComponent,
    FieldComponent
  ],
  imports: [
    BrowserModule,
    BehaviorModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
