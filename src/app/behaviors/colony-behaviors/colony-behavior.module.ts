import { NgModule } from '@angular/core'
import { COLONY_BEHAVIOR } from './colony-behavior'
import { SpawnAntBehavior } from './spawn-ant.behavior'

@NgModule({
  providers: [{
    provide: COLONY_BEHAVIOR,
    useExisting: SpawnAntBehavior,
    multi: true
  }]
})
export class ColonyBehaviorModule {}
