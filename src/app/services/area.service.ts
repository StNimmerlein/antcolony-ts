import { Injectable } from '@angular/core'
import { Area } from '../model/Area'

@Injectable({providedIn: 'root'})
export class AreaService {
  private area: Area | undefined

  createArea(width: number, height: number) {
    this.area = new Area(width, height)
  }

  getArea() {
    return this.area
  }
}
