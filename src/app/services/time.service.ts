import { Injectable } from '@angular/core'
import {
  audit,
  BehaviorSubject, bufferCount,
  combineLatest,
  debounce,
  interval,
  map,
  Observable, pairwise,
  ReplaySubject,
  scan, switchMap,
  take,
  tap,
} from 'rxjs'
import { Area, Position } from '../model/Area'
import { LifeService } from './life.service'

@Injectable({providedIn: 'root'})
export class TimeService {
  private startingArea$ = new ReplaySubject<Area>(1)
  private delay$ = new BehaviorSubject(100)
  private pause$ = new BehaviorSubject(false)

  private currentArea$ = combineLatest([
    this.startingArea$,
    this.delay$.pipe(
      switchMap(delay => this.pause$.pipe(map(pause => pause ? NaN : delay))),
      switchMap(delay => interval(delay)))
  ]).pipe(
    map(([area]) => area),
    scan((acc) => this.lifeService.iterate(acc))
  )


  constructor(private lifeService: LifeService) {
  }

  newGame(width: number, height: number) {
    this.startingArea$.next(new Area(width, height))
    return this.builder
  }

  getArea(): Observable<Area> {
    return this.currentArea$
  }

  getMsPerFrame(smoothing: number = 5): Observable<number> {
    return this.currentArea$.pipe(
      map(() => Date.now()),
      pairwise(),
      map(([oldFrameTime, newFrameTime]) => newFrameTime - oldFrameTime),
      bufferCount(smoothing, 1),
      map(frameTimes => frameTimes.reduce((a, b) => a + b)/frameTimes.length)
    )
  }

  pause() {
    this.pause$.next(true)
  }

  resume() {
    this.pause$.next(false)
  }

  setDelay(delay: number) {
    this.delay$.next(delay)
  }

  private setColony(position: Position) {
    this.startingArea$.pipe(take(1)).subscribe(area => area.addEntity({type: 'colony', collectedFood: 0}, position))
  }

  private get builder() {
    return {
      withColonyAt: (position: Position) => {
        this.setColony(position)
        return this.builder
      }
    }
  }

  private currentPos = 0
}
