import { Injectable } from '@angular/core'
import { Ant, Area, Position, PositionedEntity } from '../../model/Area'
import { AntBehavior } from './ant-behavior'

@Injectable({providedIn: 'root'})
export class AntSearchBehavior implements AntBehavior {
  terminating = true

  apply(ant: PositionedEntity<Ant>, area: Area): void {
    const xMovement = Math.floor(Math.random() * 3 - 1)
    const yMovement = Math.floor(Math.random() * 3 - 1)
    area.moveEntity(ant, new Position(xMovement, yMovement))
  }

  getPriority(): number {
    return 1
  }

  isApplicable(ant: PositionedEntity<Ant>, area: Area): boolean {
    return true
  }
}
