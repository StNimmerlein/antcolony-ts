import { Component, HostBinding, Input, OnInit } from '@angular/core'
import { Field, Food } from '../../model/Area'

@Component({
  selector: 'ant-field',
  templateUrl: './field.component.html',
  styleUrls: ['./field.component.scss'],
})
export class FieldComponent<T> implements OnInit {

  @Input()
  field: Field | undefined

  bla: T | undefined

  @HostBinding('class.grass')
  get isGrass() {
    return this.field?.terrain === 'grass'
  }

  @HostBinding('class.colony')
  get isColony() {
    return this.field?.entities.some(entity => entity.type === 'colony')
  }

  constructor() {
    let a: T | undefined
    a = this.bla
  }

  ngOnInit(): void {
  }

  get foodAmount(): number {
    return this.field?.entities
      .filter(entity => entity.type === 'food')
      .map(entity => (entity as Food).quantity)
      .reduce((sum, food) => sum + food, 0) ?? 0
  }

  hasAnt() {
    return this.field?.entities.some(entity => entity.type === 'ant')
  }

  isCarryingFood() {
    return this.field?.entities.some(entity => entity.type === 'ant' && entity.carryingFood)
  }

  hasFoodPosition() {
    return this.field?.entities.some(entity => entity.type === 'ant' && entity.rememberedFoodPosition)
  }
}
