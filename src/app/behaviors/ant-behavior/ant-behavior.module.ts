import { NgModule } from '@angular/core'
import { ANT_BEHAVIOR } from './ant-behavior'
import { AntBringFoodHomeBehavior } from './ant-bring-food-home.behavior'
import { AntDropOffFoodAtHomeBehavior } from './ant-drop-off-food-at-home.behavior'
import { AntForgetFoodPositionBehavior } from './ant-forget-food-position.behavior'
import { AntGoToFoodPositionBehavior } from './ant-go-to-food-position.behavior'
import { AntPickUpFoodBehavior } from './ant-pick-up-food.behavior'
import { AntSearchBehavior } from './ant-search.behavior'
import { AntTellOthersOfFoodBehavior } from './ant-tell-others-of-food.behavior'

@NgModule({
  providers: [{
    provide: ANT_BEHAVIOR,
    useExisting: AntSearchBehavior,
    multi: true
  }, {
    provide: ANT_BEHAVIOR,
    useExisting: AntPickUpFoodBehavior,
    multi: true
  }, {
    provide: ANT_BEHAVIOR,
    useExisting: AntDropOffFoodAtHomeBehavior,
    multi: true
  }, {
    provide: ANT_BEHAVIOR,
    useExisting: AntTellOthersOfFoodBehavior,
    multi: true
  }, {
    provide: ANT_BEHAVIOR,
    useExisting: AntGoToFoodPositionBehavior,
    multi: true
  }, {
    provide: ANT_BEHAVIOR,
    useExisting: AntForgetFoodPositionBehavior,
    multi: true
  }, {
    provide: ANT_BEHAVIOR,
    useExisting: AntBringFoodHomeBehavior,
    multi: true
  }]
})
export class AntBehaviorModule {}
