import { NgModule } from '@angular/core'
import { AntBehaviorModule } from './ant-behavior/ant-behavior.module'
import { ColonyBehaviorModule } from './colony-behaviors/colony-behavior.module'

@NgModule({
  imports: [
    AntBehaviorModule,
    ColonyBehaviorModule
  ]
})
export class BehaviorModule {}
