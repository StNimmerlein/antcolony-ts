export class Area {
  readonly width: number
  readonly height: number
  readonly fields: Field[]

  get entities(): PositionedEntity[] {
    return this.fields.flatMap((field, index) => field.entities.map(entity => ({
      entity,
      position: this.getCoordinates(index),
    })))
  }

  clone(): Area {
    const clonedField = this.fields.map(field => ({...field, entities: [...field.entities]}))
    return new Area(this.width, this.height, clonedField)
  }

  getFieldAt(position: Position) {
    return this.fields[this.getIndex(position)]
  }

  removeEntity(entity: PositionedEntity) {
    const field = this.fields[this.getIndex(entity.position)]
    const entityIndexInField = field.entities.findIndex(e => e === entity.entity)
    if (entityIndexInField === -1) {
      return
    }
    field.entities.splice(entityIndexInField, 1)
  }

  addEntity(entity: Entity, position: Position) {
    this.fields[this.getIndex(position)].entities.push(entity)
  }

  moveEntity(entity: PositionedEntity, movement: Position) {
    const field = this.fields[this.getIndex(entity.position)]
    const entityIndexInField = field.entities.findIndex(e => e === entity.entity)
    if (entityIndexInField === -1) {
      return
    }
    field.entities.splice(entityIndexInField, 1)
    const newPosition = entity.position.add(movement)
    if (this.isInField(newPosition)) {
      this.fields[this.getIndex(newPosition)].entities.push(entity.entity)
    }
  }

  private isInField(position: Position) {
    return position.row >= 0 && position.row < this.height && position.column >= 0 && position.column < this.width
  }

  getEntitiesInSquare(center: Position, radius: number): Entity[] {
    const entities: Entity[] = []
    for (let row = center.row - radius; row <= center.row + 1; row++) {
      for (let col = center.column - radius; col <= center.column + 1; col++) {
        const position = new Position(row, col)
        if (this.isInField(position)) {
          entities.push(...this.getFieldAt(position).entities)
        }
      }
    }
    return entities
  }

  constructor(width: number, height: number, fields?: Field[]) {
    this.width = width
    this.height = height
    this.fields = fields ?? Array.from(Array(width * height)).map(() => ({terrain: 'grass', entities: []}))
  }

  private getIndex({row, column}: Position) {
    return row * this.width + column
  }

  private getCoordinates(index: number): Position {
    return new Position(
      Math.floor(index / this.width),
      index % this.width,
    )
  }

  setTerrain(position: Position, terrain: Terrain) {
    this.fields[this.getIndex(position)].terrain = terrain
  }
}

export type Field = {
  terrain: Terrain,
  entities: Entity[]
}

export class Position {
  row: number
  column: number


  constructor(row: number, column: number) {
    this.row = row
    this.column = column
  }

  add(vector: Position) {
    return new Position(this.row + vector.row, this.column + vector.column)
  }

  movementTo(position: Position, errorRate: number = 0) {
    const rowDiff = position.row - this.row
    const colDiff = position.column - this.column
    const length = Math.sqrt(rowDiff * rowDiff + colDiff * colDiff)
    const movement = new Position(Math.round(rowDiff / length), Math.round(colDiff / length))
    if (Math.random() < errorRate) {
      return Position.randomizeMovement(movement)
    }
    return movement
  }

  private static randomizeMovement(movement: Position): Position {
    const coinFlip = Math.random() < 0.5
    if (movement.column === 0) {
      return new Position(movement.row, coinFlip ? 1 : -1)
    }
    if (movement.row === 0) {
      return new Position(coinFlip ? 1 : -1, movement.column)
    }
    return coinFlip ? new Position(movement.row, 0) : new Position(0, movement.column)
  }

  /*
← => ↖ ↙    (-1, 0) => (-1, 1) (-1, -1)
↖ => ← ↑    (-1, 1) => (-1, 0) (0, 1)
↑ => ↖ ↗    (0, 1) => (-1, 1) (1, 1)
→
↓
↖
↗
↘
↙
  (1, 1) => (1, 0) | (0, 1)
  (1, 0) => (1, 1) | (1, -1)
   */

  equals(other: Position | undefined | null): boolean {
    return !!other && other.row === this.row && other.column === this.column
  }
}

export type PositionedEntity<T = Entity> = {
  entity: T
  position: Position
}

export type Entity = Ant | Food | Colony


export type Ant = {
  type: 'ant'
  colonyPosition: Position
  rememberedFoodPosition?: Position
  carryingFood: boolean
}

export type Food = {
  type: 'food'
  quantity: number
}

export type Colony = {
  type: 'colony'
  collectedFood: number
}

export type Terrain = 'grass'
