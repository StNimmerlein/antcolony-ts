import { Injectable } from '@angular/core'
import { Ant, Area, Food, PositionedEntity } from '../../model/Area'
import { AntBehavior } from './ant-behavior'

@Injectable({providedIn: 'root'})
export class AntPickUpFoodBehavior implements AntBehavior {
  terminating = false
  private readonly carryingCapacity = 1

  apply(ant: PositionedEntity<Ant>, area: Area): void {
    const food = area.getFieldAt(ant.position).entities.find(entity => entity.type === 'food') as Food
    ant.entity.carryingFood = true
    food.quantity -= this.carryingCapacity
    if (food.quantity === 0) {
      area.removeEntity({entity: food, position: ant.position})
    } else {
      ant.entity.rememberedFoodPosition = ant.position
    }
  }

  getPriority(): number {
    return 110;
  }

  isApplicable(ant: PositionedEntity<Ant>, area: Area): boolean {
    return !ant.entity.carryingFood && area.getFieldAt(ant.position).entities.some(entity => entity.type === 'food');
  }
}
