import { Ant, Food, Position } from './Area'

export type AddAntAction = {
  position: Position
}

export type TakeFoodAction = {
  ant: Ant
  food: Food
  amount: number
}

export type MoveEntityAction = {}
