import { Ant, Area, PositionedEntity } from '../../model/Area'

export interface AntBehavior {
  apply(ant: PositionedEntity<Ant>, area: Area): void
  getPriority(): number

  isApplicable(ant: PositionedEntity<Ant>, area: Area): boolean
  terminating: boolean
}

export const ANT_BEHAVIOR = 'antBehavior'
