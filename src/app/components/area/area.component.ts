import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { Area, Field, Food } from '../../model/Area'

@Component({
  selector: 'ant-area',
  templateUrl: './area.component.html',
  styleUrls: ['./area.component.scss']
})
export class AreaComponent implements OnInit {

  @Input()
  area: Area | undefined

  get fields() {
    return this.area?.fields ?? []
  }

  // @HostBinding('style.grid-template-columns')
  // get gridColumnStyle() {
  //   return `repeat(${this.area?.width ?? 0}, 1fr)`
  // }

  constructor() { }

  ngOnInit(): void {
  }

  addFood(field: Field) {
    let existingFood = field.entities.find(entity => entity.type === 'food') as Food
    if (!existingFood) {
      existingFood = {type: 'food', quantity: 0}
      field.entities.push(existingFood)
    }

    existingFood.quantity += 10
  }

  getFieldWidth() {
    return `calc(100% / ${this.area?.width})`
  }
}
