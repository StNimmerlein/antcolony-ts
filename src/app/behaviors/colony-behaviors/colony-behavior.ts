import { Area, Colony, PositionedEntity } from '../../model/Area'

export interface ColonyBehavior {
  apply(colony: PositionedEntity<Colony>, area: Area): void
  getPriority(): number
  isApplicable(area: Area): boolean
}

export const COLONY_BEHAVIOR = 'colonyBehavior'
