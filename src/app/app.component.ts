import { Component } from '@angular/core'
import { Area, Position } from './model/Area'
import { TimeService } from './services/time.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  area$ = this.gameService.getArea()
  frameTimes$ = this.gameService.getMsPerFrame(10)

  constructor(private gameService: TimeService) {
    this.gameService.newGame(100, 50).withColonyAt(new Position(5, 10))
  }

  pause() {
    this.gameService.pause()
  }

  resume() {
    this.gameService.resume()
  }

  updateDelay($event: Event) {
    this.gameService.setDelay(Number(($event.target as HTMLInputElement).value))
  }
}
