import { Injectable } from '@angular/core'
import { Ant, Area, PositionedEntity } from '../../model/Area'
import { AntBehavior } from './ant-behavior'

@Injectable({providedIn: 'root'})
export class AntTellOthersOfFoodBehavior implements AntBehavior {
  terminating = false
  private readonly radius = 0

  apply(ant: PositionedEntity<Ant>, area: Area): void {
    const neighbors = area.getEntitiesInSquare(ant.position, this.radius)
    neighbors.forEach(neighbor => {
      if (neighbor !== ant.entity && neighbor.type === 'ant' && !neighbor.rememberedFoodPosition) {
        neighbor.rememberedFoodPosition = ant.entity.rememberedFoodPosition
      }
    })
  }

  getPriority(): number {
    return 99;
  }

  isApplicable(ant: PositionedEntity<Ant>, area: Area): boolean {
    return !!ant.entity.rememberedFoodPosition;
  }
}
