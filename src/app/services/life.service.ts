import { Inject, Injectable } from '@angular/core'
import { ANT_BEHAVIOR, AntBehavior } from '../behaviors/ant-behavior/ant-behavior'
import { COLONY_BEHAVIOR, ColonyBehavior } from '../behaviors/colony-behaviors/colony-behavior'
import { Ant, Area, Colony, PositionedEntity } from '../model/Area'

@Injectable({providedIn: 'root'})
export class LifeService {

  constructor(
    @Inject(COLONY_BEHAVIOR) private colonyBehaviors: ColonyBehavior[],
    @Inject(ANT_BEHAVIOR) private antTimeConsumingBehaviors: AntBehavior[],
  ) {
  }

  iterate(area: Area): Area {
    area.entities.forEach(entity => {
      if (entity.entity.type === 'colony') {
        const colony = entity as PositionedEntity<Colony>
        this.colonyBehaviors
          .sort((a, b) => b.getPriority() - a.getPriority())
          .filter(behavior => behavior.isApplicable(area))
          .forEach(behavior => behavior.apply(colony, area))
      }
      if (entity.entity.type === 'ant') {
        const ant = entity as PositionedEntity<Ant>
        const sortedBehaviors = this.antTimeConsumingBehaviors
          .sort((a, b) => b.getPriority() - a.getPriority())
        for (const behavior of sortedBehaviors) {
          if (behavior.isApplicable(ant, area)) {
            behavior.apply(ant, area)

            if (behavior.terminating) {
              break;
            }
          }
        }
      }
    })
    return area
  }

}
