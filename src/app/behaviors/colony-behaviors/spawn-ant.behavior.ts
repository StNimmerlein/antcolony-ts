import { Injectable } from '@angular/core'
import { Area, Colony, PositionedEntity } from '../../model/Area'
import { ColonyBehavior } from './colony-behavior'

@Injectable({providedIn: 'root'})
export class SpawnAntBehavior implements ColonyBehavior {
  private readonly probability = 0.1

  apply(colony: PositionedEntity<Colony>, area: Area): void {
    area.addEntity({type: 'ant', carryingFood: false, colonyPosition: colony.position}, colony.position)
  }

  getPriority(): number {
    return 1
  }

  isApplicable(area: Area): boolean {
    return Math.random() < this.probability
  }
}
