import { Injectable } from '@angular/core'
import { Ant, Area, Colony, PositionedEntity } from 'src/app/model/Area'
import { AntBehavior } from './ant-behavior'

@Injectable({providedIn: 'root'})
export class AntDropOffFoodAtHomeBehavior implements AntBehavior {
  terminating = false

  apply(ant: PositionedEntity<Ant>, area: Area): void {
    ant.entity.carryingFood = false
    const colony = area.getFieldAt(ant.position).entities.find(entity => entity.type === 'colony') as Colony
    colony.collectedFood++
  }

  getPriority(): number {
    return 100;
  }

  isApplicable(ant: PositionedEntity<Ant>, area: Area): boolean {
    return ant.entity.carryingFood && ant.position.equals(ant.entity.colonyPosition);
  }

}
