import { Injectable } from '@angular/core'
import { Ant, Area, PositionedEntity } from 'src/app/model/Area'
import { AntBehavior } from './ant-behavior'

@Injectable({providedIn: 'root'})
export class AntBringFoodHomeBehavior implements AntBehavior {
  terminating = true

  apply(ant: PositionedEntity<Ant>, area: Area): void {
    area.moveEntity(ant, ant.position.movementTo(ant.entity.colonyPosition))
  }

  getPriority(): number {
    return 2
  }

  isApplicable(ant: PositionedEntity<Ant>, area: Area): boolean {
    return ant.entity.carryingFood
  }

}
