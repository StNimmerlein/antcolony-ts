import { Injectable } from '@angular/core'
import { Ant, Area, PositionedEntity } from '../../model/Area'
import { AntBehavior } from './ant-behavior'

@Injectable({providedIn: 'root'})
export class AntGoToFoodPositionBehavior implements AntBehavior {
  terminating = true

  apply(ant: PositionedEntity<Ant>, area: Area): void {
    area.moveEntity(ant, ant.position.movementTo(ant.entity.rememberedFoodPosition!))
  }

  getPriority(): number {
    return 5;
  }

  isApplicable(ant: PositionedEntity<Ant>, area: Area): boolean {
    return !ant.entity.carryingFood && !!ant.entity.rememberedFoodPosition;
  }
}
